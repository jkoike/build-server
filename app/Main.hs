module Main where

import Network.Wai.Handler.Warp
import System.Process
import System.Exit

import Api

buildElm :: CreateProcess
buildElm = (proc "stack" ["exec", "--", "elm-make", "--output=dist/main.js",  "--warn", "--debug", "elm/Main.elm"] )

call :: CreateProcess -> IO ()
call proc = do
  (_, _, _, app) <- createProcess proc
  ExitSuccess <- waitForProcess app
  return ()

main :: IO ()
main =  do
  putStrLn "Building frontend"
  call buildElm
  putStrLn "Making api js files"
  writeJsFiles
  putStrLn "Ready to go!"
  run 8080 app
