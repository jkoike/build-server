module Model exposing (..)
import Dict exposing (Dict)
import Navigation
import Json.Encode as E
import Json.Decode as D
import Http

type alias App =
  { name    : String
  , remote  : String
  , state   : AppState
  , restart : Bool
  , start   : Bool
  , env     : Dict String String
  , nextEnv : String
  }

type alias Model = 
  { location    : Navigation.Location
  , notifyQueue : Dict Int (NotificationPriority, String)
  , notifyId    : Int
  , appList     : Dict String App
  , appFocus    : AppFocus
  , autoUpdate  : Bool
  }

type AppFocus = Detail String
              | NewApp App

type NotificationPriority = Success
                          | Info
                          | Warning
                          | Error

type AppState = Init
              | Download
              | Build
              | Run
              | Paused
              | Dead

type Msg = Get     String
         | Pause   String
         | Upgrade String
         | Sync    String    App
         | Update  (App -> App)
         | Add     App
         | AppGet     (Result Http.Error (String, App))
         | AppPause   String (Result Http.Error String)
         | AppUpgrade String (Result Http.Error String)
         | AppUpdate  (Result Http.Error (String, App))
         | AppGetAll  (Result Http.Error (List (String, App)))
         | DeleteNotification Int
         | Notify NotificationPriority String
         | Focus    AppFocus
         | Navigate Navigation.Location
         | GetAll

newApp : App
newApp = 
  { name    = ""
  , remote  = ""
  , state   = Init
  , restart = False
  , start   = True
  , env     = Dict.empty
  , nextEnv = ""
  }

parseState : String -> D.Decoder AppState
parseState state =
  case state of
    "Init"     -> D.succeed Init
    "Download" -> D.succeed Download
    "Build"    -> D.succeed Build
    "Run"      -> D.succeed Run
    "Paused"   -> D.succeed Paused
    "Dead"     -> D.succeed Dead
    _          -> D.fail "Unrecognized state"

stateClass : AppState -> String
stateClass state = case state of
  Init     -> "alert alert-info"
  Download -> "alert alert-info"
  Build    -> "alert alert-info"
  Run      -> "alert alert-success"
  Paused   -> "alert alert-warning"
  Dead     -> "alert alert-danger"

parseApp : D.Decoder (String, App)
parseApp = D.map2 (,)
  ( D.field "appId" D.string )
  ( D.map7 App
    (D.field "appName" D.string)
    (D.field "appRemote" D.string)
    (D.field "appStatus" 
      ( D.andThen parseState D.string )
    )
    (D.field "appRestart" D.bool)
    (D.succeed True)
    (D.field "appEnv" 
      (D.dict D.string)
    )
    (D.succeed "")
  )

showPriority : NotificationPriority -> String
showPriority priority = case priority of
  Success -> "alert-success"
  Info    -> "alert-info"
  Warning -> "alert-warning"
  Error   -> "alert-danger"

encodeApp : App -> E.Value
encodeApp app = E.object
  [ ("appName",    E.string app.name)
  , ("appRemote",  E.string app.remote)
  , ("appRestart", E.bool   app.restart)
  , ("appStart",   E.bool   app.start)
  , ("appEnv",     E.list   
      (List.map 
        (\(a, b) -> E.list [E.string a, E.string b])
        (Dict.toList app.env)
      )
    )
  ]
