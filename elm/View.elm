module View exposing (..)

import Dict exposing (Dict)
import Model exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (..)

view : Model -> Html Msg
view model = body [] 
  [ Html.Lazy.lazy viewNotifications model.notifyQueue
  , appList model
  , renderAppFocus model
  ]

inputField : String -> String -> (String -> Msg) -> Html Msg
inputField label contents f = li
  [ class "list-group-item"
  ]
  [ div
    [ class "input-group"
    ]
    [ span
      [ class "input-group-addon"
      ]
      [ text label
      ]
    , input
      [ class "form-control"
      , type_ "text"
      , value contents
      , onInput f
      ]
      []
    ]
  ]

inputCheckbox : String -> Bool -> Msg -> Html Msg
inputCheckbox formLabel check f = li
  [ classList
    [ ("list-group-item", True)
    , ("list-group-item-success", check)
    , ("list-group-item-danger", not check)
    ]
  , onClick f
  ]
  [ label
    [ 
    ]
    [ text formLabel
    , input
      [ class "form-control"
      , type_ "checkbox"
      , style
        [ ("display", "none")
        ]
      , checked check
      ]
      []
    ]
  ]

renderSubmitButton : Model -> Html Msg
renderSubmitButton model =
  let
    submitAction = case model.appFocus of
      NewApp app  -> Add  app
      Detail uuid -> case Dict.get uuid model.appList of
        Just app  -> Sync uuid app
        Nothing   -> Notify Error "App does not exist"
  in
    a
      [ onClick submitAction
      , class "btn btn-primary"
      ]
      [ text "Submit"
      ]

renderAppFocus : Model -> Html Msg
renderAppFocus model =
  let
    maybeUuid = case model.appFocus of
      NewApp _ -> Nothing
      Detail uid -> Just uid
    maybeApp = case model.appFocus of
      NewApp app  -> Just app
      Detail uuid -> Dict.get uuid model.appList
  in
   case maybeApp of
     Nothing  -> text "Something went really wrong"
     Just app -> Html.form []
      ([ div
        [ class (stateClass app.state)
        ]
        [ text (toString app.state)
        ]
      , ul
        [ class "form-group"
        , class "list-group"
        ]
        [ inputField    "App Name:"       
          app.name    (\newName   -> Update (\app -> {app | name = newName}))
        , inputField    "Url:"            
          app.remote  (\newRemote -> Update (\app -> {app | remote = newRemote}))
        , inputCheckbox "Restart on fail" 
          app.restart (Update (\app -> {app | restart = not app.restart}))
        , inputCheckbox "Start on sync"   
          app.start   (Update (\app -> {app | start = not app.start} ))
        , viewEnvVars app
        , renderSubmitButton model
        ] 
      ]
      ++ (case maybeUuid of
        Just uuid -> [pauseButton uuid]
        Nothing   -> []
        )
      )

pauseButton : String -> Html Msg
pauseButton uuid =
  a
  [ class "btn btn-warning"
  , onClick (Pause uuid)
  ]
  [ text "Pause"
  ]
        
syncAppNav : Html Msg
syncAppNav = li
  [ attribute "role" "navigation"
  , onClick GetAll
  ]
  [ a
    [ class "fa fa-refresh"
    ]
    []
  ]

appList : Model -> Html Msg
appList model = ul
  [ class "nav nav-tabs"
  ]
  (syncAppNav
    ::(newAppNav model.appFocus)
    ::(Dict.foldl (viewAppList model.appFocus) [] model.appList)
  )

viewAppList : AppFocus -> String -> App -> List (Html Msg) -> List (Html Msg)
viewAppList focus uuid app xs = (viewAppNav focus uuid app)::xs

viewAppNav : AppFocus -> String -> App -> Html Msg
viewAppNav focus uuid app = 
  let
    focused = case focus of
      Detail appId -> uuid == appId
      _ -> False
  in
    li
      [ classList
        [ ("active", focused)
        ]
      , attribute "role" "navigation"
      ]
      [ a
        [ onClick (Focus (Detail uuid))
        ]
        [ text app.name
        ]
      ]

newAppNav : AppFocus -> Html Msg
newAppNav focus =
  let nav_button = i [ class "fa fa-plus-square-o" ] [] in
    case focus of
      Detail _ -> li
        [ attribute "role" "navigation"
        ]
        [ a
          [ onClick (Focus (NewApp newApp))
          ]
          [ nav_button
          ]
        ]
      NewApp app -> li
        [ attribute "role" "navigation"
        , class "active"
        ]
        [ a
          [ onClick (Focus (NewApp app))
          ]
          [ nav_button
          ]
        ]

viewEnvVars : App -> Html Msg
viewEnvVars app =
  li
    [ class "list-group-item"
    ]
    (Dict.foldl (envVar app) [] app.env
    ++
      [ div
        [ class "input-group"
        ]
        [ input
          [ class "form-control"
          , type_ "text"
          , onInput (\var -> Update 
              (\app -> {app | nextEnv = var })
            )
          ]
          []
        , span
          [ class "input-group-btn"
          ]
          [ a
            [ class "btn btn-success"
            , onClick (Update (\app -> {app 
              | env = Dict.insert app.nextEnv "" app.env
              , nextEnv = ""
              }))
            ]
            [ span
              [ class "fa fa-check"
              ]
              []
            ]
          ]
        ]
      ]
    )

envVar : App -> String -> String -> List (Html Msg) -> List (Html Msg)
envVar app label contents xs = 
  ( div
    [ class "input-group"
    ]
    [ span
      [ class "input-group-addon"
      ]
      [ text label
      ]
    , input
      [ class "form-control"
      , type_ "text"
      , value contents
      , onInput 
        (\new -> Update
          (\app -> { app
            | env = Dict.update label (\_ -> Just new) app.env}
          )
        )
      ]
      []
    , span
      [ class "input-group-btn"
      ]
      [ span
        [ class "btn btn-danger"
        , onClick (Update (\app -> {app | env = Dict.remove label app.env}))
        ]
        [ span
          [ class "fa fa-trash"
          ]
          []
        ]
      ]
    ]
  )::xs

viewNotifications : Dict Int (NotificationPriority, String) -> Html Msg
viewNotifications notifications = div
  [ class "notification-bar"
  ]
  (List.map notify (Dict.toList notifications))

notify : (Int, (NotificationPriority, String)) -> Html Msg
notify (index, (priority, msg)) =
  div
    [ class (showPriority priority)
    , class "alert"
    , class "fade in"
    , id ("notification-" ++ toString index)
    , attribute "aria-label" "close"
    ]
    [ text msg
    , a 
      [ onClick (DeleteNotification index)
      , class "close"
      ]
      [ i
        [ class "fa fa-window-close"
        , attribute "aria-hidden" "true"
        ]
        [
        ]
      ]
    ]
