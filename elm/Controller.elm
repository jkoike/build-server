module Controller exposing (..)

import Dict exposing (..)
import Http
import Json.Decode as D
import Json.Encode as E
import Model exposing (..)
import Navigation
import Process
import Task
import Time

subscriptions : Model -> Sub Msg
subscriptions model =
  if model.autoUpdate
     then
       case model.appFocus of
         NewApp _    -> Time.every Time.second (\_ -> GetAll)
         Detail uuid -> Time.every Time.second (\_ -> Get uuid)
     else Sub.none

init : Navigation.Location -> ( Model, Cmd Msg )
init home =
  ( { location = home
    , notifyQueue = Dict.empty
    , notifyId = 0
    , appList = Dict.empty
    , appFocus = NewApp newApp
    , autoUpdate = False
    }
  , Cmd.none
  )

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Get uuid ->
      ( model
      , Http.send AppGet (Http.get ("/app/" ++ uuid) parseApp)
      )
    Pause uuid ->
      ( model
      , Http.send (AppPause uuid) (Http.post ("/app/" ++ uuid) Http.emptyBody (D.succeed "OK"))
      )
    Upgrade uuid ->
      ( model
      , Http.send (AppUpgrade uuid) (Http.post ("/app/" ++ uuid) Http.emptyBody (D.succeed "OK"))
      )
    Sync uuid app ->
      ( model
      , Http.send AppUpdate (Http.post ("/app/" ++ uuid) (Http.jsonBody (encodeApp app)) parseApp)
      )
    Update f ->
      case model.appFocus of
        Detail uuid ->
          ( { model | appList = Dict.update uuid (Maybe.map f) model.appList }
          , Cmd.none
          )
        NewApp new ->
          ( { model | appFocus = NewApp (f new) }
          , Cmd.none
          )
    GetAll -> 
      ( model
      , Http.send AppGetAll (Http.get "/app/" (D.list parseApp))
      )
    Add app ->
      ( model
      , Http.send AppUpdate (Http.post "/app" (Http.jsonBody (encodeApp app)) parseApp)
      )
    AppGet (Ok (uuid, app)) ->
      ( { model | appList = insert uuid app model.appList }
      , Cmd.none
      )
    AppPause uuid (Ok "OK") ->
      ( model
      , Cmd.map (\_ -> Get uuid) Cmd.none
      )
    AppUpgrade uuid (Ok "OK") ->
      ( model
      , Cmd.map (\_ -> Get uuid) Cmd.none
      )
    AppUpdate (Ok (uuid, app)) ->
      ( { model | appList = Dict.insert uuid app model.appList }
      , Cmd.none
      )
    AppGetAll (Ok apps) ->
      ( { model | appList = Dict.fromList apps }
      , Cmd.none
      )
    Notify priority msg ->
      ( { model | notifyQueue = Dict.insert model.notifyId (priority, msg) model.notifyQueue, notifyId = model.notifyId + 1  }
      , Task.perform (\_ -> DeleteNotification model.notifyId) (Process.sleep (3 * Time.second))
      )
    DeleteNotification index ->
      ( { model | notifyQueue = Dict.remove index model.notifyQueue }
      , Cmd.none
      )
    Focus app ->
      ( { model | appFocus = app }
      , Cmd.none
      )
    _ -> (model, Cmd.none)
