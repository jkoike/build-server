module Process where
  import System.Process
  import Network.URI
  import System.IO.Temp

  download :: URI -> CreateProcess
  download url = proc "git" ["clone", show url, "."]

  update :: CreateProcess
  update = proc "git" ["pull"]

  build :: CreateProcess
  build = proc "stack" ["build", "--resolver", "lts-8.2"]

  run :: CreateProcess
  run = proc "stack" ["exec", "--resolver", "lts-8.2", "app"]

  mkdir :: FilePath -> CreateProcess
  mkdir path = proc "mkdir" [path]
