{-# OPTIONS_GHC -funbox-strict-fields #-}
{-# LANGUAGE DeriveGeneric #-}
module Util where
  import GHC.Generics
  import Data.Aeson
  import Data.Attoparsec.ByteString.Char8
  import Data.ByteString.Char8 (pack)
  import Data.Maybe
  import System.Process.Internals

  divOut :: Int -> [Int] -> [Int]
  divOut x (y:ys)
    | x > y     = go ys $ divMod x y
    | otherwise = [x]
    where
      go :: [Int] -> (Int, Int) -> [Int]
      go ys (d, m) = m:divOut d ys
  divOut x []   = [x]

  data PidState = PidRunning | PidSleeping | PidWaiting
                | PidZombie  | PidStopped  | PidPaging  
                | PidDead    | PidWakekill | PidWaking
                | PidParked  deriving (Show, Generic)

  instance ToJSON PidState

  -- Why, linux... why...
  data PidStat = Stat
    { pid         :: !Int
    , comm        :: !String
    , state       :: !PidState
    , ppid        :: !Int
    , pgrp        :: !Int
    , session     :: !Int
    , tty_nr      :: !Int
    , tpgid       :: !Int
    , flags       :: !Int
    , minflt      :: !Int
    , cminflt     :: !Int
    , majflt      :: !Int
    , cmajflt     :: !Int
    , utime       :: !Int
    , stime       :: !Int
    , cutime      :: !Int
    , cstime      :: !Int
    , priority    :: !Int
    , nice        :: !Int
    , num_threads :: !Int
    , itrealvalue :: !Int
    , starttime   :: !Int
    , vsize       :: !Int
    , rss         :: !Int
    , rsslim      :: !Int
    , startcode   :: !Int
    , endcode     :: !Int
    , startstack  :: !Int
    , ktskep      :: !Int
    , ktskeip     :: !Int
    , signal      :: !Int
    , blocked     :: !Int
    , sigignore   :: !Int
    , sigcatch    :: !Int
    , wchan       :: !Int
    , nswap       :: !Int
    , cnswap      :: !Int
    , exit_signal :: !Int
    , processor   :: !Int
    , rt_priority :: !Int
    , policy      :: !Int
    , delayacct_blkio_ticks :: !Int
    , guest_time  :: !Int
    , cguest_time :: !Int
    , start_data  :: !Int
    , end_data    :: !Int
    , start_brk   :: !Int
    , arg_start   :: !Int
    , arg_end     :: !Int
    , env_start   :: !Int
    , env_end     :: !Int
    , exit_code   :: !Int
    } deriving (Show, Generic)

  instance ToJSON PidStat

  readStat :: PHANDLE -> IO PidStat
  readStat process = do
    print process
    putStrLn =<< readFile ("/proc/" ++ show process ++ "/stat")
    either error return =<< parseOnly parsePidStat <$> (pack <$> readFile 
       ("/proc/"++ show process ++"/stat"))
  {-# NOINLINE readStat #-}

  parsePidStat :: Parser PidStat
  parsePidStat =  Stat
              <$> ((skipSpace *> signed decimal) <?> "pid")
              <*> ((skipSpace *> 
                (  char '(' 
                *> (show <$> takeTill (==')'))
                <* char ')'
                )) <?> "comm")
              <*> ((skipSpace *> choice
                [ char 'R' *> pure PidRunning
                , char 'S' *> pure PidSleeping
                , char 'D' *> pure PidWaiting
                , char 'Z' *> pure PidZombie
                , char 'T' *> pure PidStopped
                , char 't' *> pure PidStopped
                , char 'W' *> pure PidPaging
                , char 'X' *> pure PidDead
                , char 'x' *> pure PidDead
                , char 'K' *> pure PidWakekill
                , char 'W' *> pure PidWaking
                , char 'P' *> pure PidParked
                ]) <?> "state")
              <*> ((skipSpace *> signed decimal) <?> "ppid")
              <*> ((skipSpace *> signed decimal) <?> "pgrp")
              <*> ((skipSpace *> signed decimal) <?> "session")
              <*> ((skipSpace *> signed decimal) <?> "tty_nr")
              <*> ((skipSpace *> signed decimal) <?> "tpgid")
              <*> ((skipSpace *> signed decimal) <?> "flags")
              <*> ((skipSpace *> signed decimal) <?> "minflt")
              <*> ((skipSpace *> signed decimal) <?> "cminflt")
              <*> ((skipSpace *> signed decimal) <?> "majflt")
              <*> ((skipSpace *> signed decimal) <?> "cmajflt")
              <*> ((skipSpace *> signed decimal) <?> "utime")
              <*> ((skipSpace *> signed decimal) <?> "stime")
              <*> ((skipSpace *> signed decimal) <?> "cutime")
              <*> ((skipSpace *> signed decimal) <?> "cstime")
              <*> ((skipSpace *> signed decimal) <?> "priority")
              <*> ((skipSpace *> signed decimal) <?> "nice")
              <*> ((skipSpace *> signed decimal) <?> "num_threads")
              <*> ((skipSpace *> signed decimal) <?> "itrealvalue")
              <*> ((skipSpace *> signed decimal) <?> "starttime")
              <*> ((skipSpace *> signed decimal) <?> "vsize")
              <*> ((skipSpace *> signed decimal) <?> "rss")
              <*> ((skipSpace *> signed decimal) <?> "rsslim")
              <*> ((skipSpace *> signed decimal) <?> "startcode")
              <*> ((skipSpace *> signed decimal) <?> "endcode")
              <*> ((skipSpace *> signed decimal) <?> "startstack")
              <*> ((skipSpace *> signed decimal) <?> "kstkesp")
              <*> ((skipSpace *> signed decimal) <?> "kstkeip")
              <*> ((skipSpace *> signed decimal) <?> "signal")
              <*> ((skipSpace *> signed decimal) <?> "blocked")
              <*> ((skipSpace *> signed decimal) <?> "sigignore")
              <*> ((skipSpace *> signed decimal) <?> "sigcatch")
              <*> ((skipSpace *> signed decimal) <?> "wchan")
              <*> ((skipSpace *> signed decimal) <?> "nswap")
              <*> ((skipSpace *> signed decimal) <?> "cnswap")
              <*> ((skipSpace *> signed decimal) <?> "exit_signal")
              <*> ((skipSpace *> signed decimal) <?> "processor")
              <*> ((skipSpace *> signed decimal) <?> "rt_priority")
              <*> ((skipSpace *> signed decimal) <?> "policy")
              <*> ((skipSpace *> signed decimal) <?> "delayacct_blkio_ticks")
              <*> ((skipSpace *> signed decimal) <?> "guest_time")
              <*> ((skipSpace *> signed decimal) <?> "cguest_time")
              <*> ((skipSpace *> signed decimal) <?> "start_data")
              <*> ((skipSpace *> signed decimal) <?> "end_data")
              <*> ((skipSpace *> signed decimal) <?> "start_brk")
              <*> ((skipSpace *> signed decimal) <?> "arg_start")
              <*> ((skipSpace *> signed decimal) <?> "arg_end")
              <*> ((skipSpace *> signed decimal) <?> "env_start")
              <*> ((skipSpace *> signed decimal) <?> "env_end")
              <*> ((skipSpace *> signed decimal) <?> "exit_code")
