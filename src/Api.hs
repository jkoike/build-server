{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
module Api where
  import Control.Monad.Reader

  import Data.HashTable.IO
  import Data.Text.IO as T
  import Servant
  import Servant.Utils.StaticFiles
  import Servant.JS
  import System.IO.Unsafe
  import Data.UUID

  import Lib
  import Type
  import Util
  
  vault :: BasicHashTable UUID AppState
  vault = unsafePerformIO new

  appMToHandler :: AppM :~> Handler
  appMToHandler = Nat go
    where
      go :: forall a. AppM a -> Handler a
      go r = lift $ runAppM r vault

  type AppApi =  "app" :> Get '[JSON] [AppState]
            :<|> "app" :> Capture "app" UUID        :> Get  '[JSON] AppState
            :<|> "app" :> Capture "app" UUID        :> "pause"  :> PostAccepted '[JSON] ()
            :<|> "app" :> Capture "app" UUID        :> "update" :> PostAccepted '[JSON] ()
            :<|> "app" :> Capture "app" UUID        :> "stats"  :> Get '[JSON] PidStat
            :<|> "app" :> Capture "app" UUID        :> ReqBody '[JSON] CreateApp :> Post '[JSON] AppState
            :<|> "app" :> ReqBody '[JSON] CreateApp :> Post '[JSON] AppState

  appServer :: ServerT AppApi AppM
  appServer =  listApps
          :<|> listApp
          :<|> pauseApp
          :<|> upgradeApp
          :<|> statApp
          :<|> updateApp
          :<|> makeApp

  appApi :: Proxy AppApi
  appApi =  Proxy

  writeJsFiles :: IO ()
  writeJsFiles = do
    T.writeFile "dist/api.js" $ jsForAPI appApi vanillaJS
  
  app :: Application
  app = serve (Proxy :: Proxy (AppApi :<|> Raw)) $ (enter appMToHandler appServer) :<|> serveDirectory "dist/"
